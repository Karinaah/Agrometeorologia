# Agrometeorologia

Este projeto tem o objetivo de coletar e armazenar dados climaticos.

## Documentos do projeto 

### Primeira entrega 
* [Project Charter](https://gitlab.com/BDAg/Agrometeorologia/wikis/Project_charter)
* [Cronograma](https://gitlab.com/BDAg/Agrometeorologia/wikis/Cronograma)
* [Equipe](https://gitlab.com/BDAg/Agrometeorologia/wikis/Equipe)
* [Mapa de Conhecimento](https://gitlab.com/BDAg/Agrometeorologia/wikis/Matriz_de_Conhecimento)
* [Matriz de Habilidades](https://gitlab.com/BDAg/Agrometeorologia/wikis/Matriz_de_Habilidades_da_Equipe)

### Segunda entrega
* [MVP]()
* [Arquitetura da soloção]()
* [Modelagem de dados]()
* [Mapa de definição tecnologica]()
* [Montagem do abiente de desenvolvimento]()
* [Lista de Funcionalidades]()